#ifndef _CONFIG_H_
#define _CONFIG_H_

/****************************************************************
 * This file must be copied to "config.h" in this directory and
 * edited accordingly.
 ****************************************************************/

// Change this to  your WLAN name and password
#define STASSID 	"WLAN name here"
#define STAPSK  	"WLAN password here"

// please set this to the hostname and port you want to use
#define HOSTNAME	"gemini"
#define HOSTPORT	1965

#ifndef USE_EC

// PLEASE USE YOUR OWN CERTIFICATE HERE
// The one provided is just for compilation testing
// DON'T USE THIS ON YOUR DEVICES!

// RSA CERT

// The server's private key which must be kept secret
const char server_private_key[] PROGMEM = R"EOF(
-----BEGIN PRIVATE KEY-----
MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALm2pxA/aVmz9qDE
8KbN47usvrGs+kTj4KfV0s4C+nk8I1KGuOVxgy79x+oEnxqB65hHSv/P52NkUBZS
aEO7nkRPO+Qud5C2X60zOOrHO6pSGQeKxiSRyDzz5UgdPd/gO8c1ul9LBNM1ol6H
u2IB8s+BP4Yo1cHzkp+S5DoIpD0hAgMBAAECgYAPOb9WoKxFc3Cr25mTQ1wBXfdA
X9sg3MHlYK1owGvClsCamnr8LKNBrgK+1+QnazFiia3HbUceKfI68kSkEPS3pzZq
oPMal6yTIM48Xt2K8XnnZIcIr17KuOhWwjMzUItx0jRyDzgpN9fYQNGFV/UDBDqU
ZwehQx9+HjesXuk0xQJBAODFrIZGe7/WMiVHl+nLkzz94dDFOgo6nLyubQrdZqAK
QG4PonpIc0XiEHeO9clP6McMxytM+S73SO2v3DxRIOsCQQDTg83Rj5XCgAI0/qf0
jNRgreb068BYBx4oTFwePhi7m2zgntx30Y5kDRroZnKJC5R6wOP7Y4LuZ1h4PK0l
3vcjAkAqbAR4vzrB5RpHwPBh9sMOQthov6CJp7BHSCBaaZjh4a1xWFARBjWDXfw/
pGKtIQDM+JNqj7eD7rX77jIBzEoLAkEAgtEQ+Zw5fosXUllx3UFAlO7ZWf94/bih
QUFA8/9p4pZVN3NpXb3AuEaWrse17Oa5lVY5aqB4OSS3xd2ZJUQKSwJAVWWPn3L9
+A5YStXZOn8zWsZAtEATGb8Fw7w0CVGCcUDACB4Dbrr5/RAGCzRCJsfErzhI6oCb
i29B4pcqkvl3Sw==
-----END PRIVATE KEY-----
)EOF";

// The server's public certificate which must be shared
const char server_cert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDAjCCAmugAwIBAgIUcqAwjRBgHCQa6CU8TwkfR/pAg0QwDQYJKoZIhvcNAQEL
BQAwgZIxCzAJBgNVBAYTAkRFMQwwCgYDVQQIDANOUlcxFDASBgNVBAcMC0R1ZXNz
ZWxkb3JmMRowGAYDVQQKDBFlc3A4MjY2LWdlbXNlcnZlcjEQMA4GA1UECwwHQXJk
dWlubzEPMA0GA1UEAwwGZ2VtaW5pMSAwHgYJKoZIhvcNAQkBFhFmYWJAZm9vYnVj
a2V0Lnh5ejAeFw0yMTA5MTQxNjI0NThaFw0yMjA5MTQxNjI0NThaMIGSMQswCQYD
VQQGEwJERTEMMAoGA1UECAwDTlJXMRQwEgYDVQQHDAtEdWVzc2VsZG9yZjEaMBgG
A1UECgwRZXNwODI2Ni1nZW1zZXJ2ZXIxEDAOBgNVBAsMB0FyZHVpbm8xDzANBgNV
BAMMBmdlbWluaTEgMB4GCSqGSIb3DQEJARYRZmFiQGZvb2J1Y2tldC54eXowgZ8w
DQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALm2pxA/aVmz9qDE8KbN47usvrGs+kTj
4KfV0s4C+nk8I1KGuOVxgy79x+oEnxqB65hHSv/P52NkUBZSaEO7nkRPO+Qud5C2
X60zOOrHO6pSGQeKxiSRyDzz5UgdPd/gO8c1ul9LBNM1ol6Hu2IB8s+BP4Yo1cHz
kp+S5DoIpD0hAgMBAAGjUzBRMB0GA1UdDgQWBBTG3voJrBMzYrOadXSUscxIxirU
XzAfBgNVHSMEGDAWgBTG3voJrBMzYrOadXSUscxIxirUXzAPBgNVHRMBAf8EBTAD
AQH/MA0GCSqGSIb3DQEBCwUAA4GBAFXBHUf7AZ6WcP+LbKIX2Ti7AWhD0/hL9817
cdVTTsOzlgRx3WE0xmy8JSKjalfbRbIBV7QigfYMCZqStrdwbRSVXDmrjyTHw9/N
5kWqWJEgqgqxWRai/qmRMjuVyV1WlPz+uCu6g5v0CYyeRD7sLMYy5RLM2WZl7DQo
TeIkPNPU
-----END CERTIFICATE-----
)EOF";

#else

// EC CERT

// The server's private key which must be kept secret
const char server_private_key[] PROGMEM = R"EOF(
-----BEGIN EC PARAMETERS-----
BggqhkjOPQMBBw==
-----END EC PARAMETERS-----
-----BEGIN PRIVATE KEY-----
MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgj0W2YgoWgTUNWP8T
v1ZOKyX3SEmbAq4R7BjdOwLiS6ChRANCAAT36c8qmZHI9WfaG2MhLHU/dRmncu7r
RoW79EZQSHSmvNnInsQ952OQEgmW9miLw/CnHWubK8V7zaxKUqz557W7
-----END PRIVATE KEY-----
)EOF";

// The server's public certificate which must be shared
const char server_cert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIBdzCCAR2gAwIBAgIUNYg0ToQPSnGoBslhKxxS1k8+ysswCgYIKoZIzj0EAwIw
ETEPMA0GA1UEAwwGZ2VtaW5pMB4XDTIxMDkxNDE2NDczM1oXDTMxMDkxMjE2NDcz
M1owETEPMA0GA1UEAwwGZ2VtaW5pMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE
9+nPKpmRyPVn2htjISx1P3UZp3Lu60aFu/RGUEh0przZyJ7EPedjkBIJlvZoi8Pw
px1rmyvFe82sSlKs+ee1u6NTMFEwHQYDVR0OBBYEFGDUy0ERhsA3QfRjpvws8f1Q
5fy4MB8GA1UdIwQYMBaAFGDUy0ERhsA3QfRjpvws8f1Q5fy4MA8GA1UdEwEB/wQF
MAMBAf8wCgYIKoZIzj0EAwIDSAAwRQIhAKkucBU6u0oWrJz4GKOX/JFQqZbelZW7
BKF+tfkXoTOcAiBi6iPHXGHlNrLiNpZhS1Q+z9QKVTF9r1JjvWtvAASmSw==
-----END CERTIFICATE-----
)EOF";

#endif // USE_EC

#endif // _CONFIG_H_

