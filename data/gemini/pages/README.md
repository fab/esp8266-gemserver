# esp8266-gemserver
A Gemini server for the ESP8266 microcontroller

This is a small private project only to tinker around with the ESP8266
microprocessor and it's at an very early stage. So you *WILL* run into 
bugs.

## How to compile
1. Set up your Arduino IDE for support of the ESP8266 microcontroller.
Look at https://github.com/esp8266/Arduino .

2. Install the [ESP8266LittleFS tool](https://github.com/earlephilhower/arduino-esp8266littlefs-plugin/releases)
into the "tools" folder in your Arduino sketchbook directory.

3. Copy the "config.dist.h" file to "config.h" and edit it accordingly
(Set Wifi Credentials, hostname, port and the TLS cert/key)

4. Open the "esp8266-gemserver.ino" file in your Arduino IDE.

5. Select your esp8266 board (e.g. "NodeMCU 1.0 (ESP-12E Module)") and
set the flash size accordingly. Also set the speed of the ESP8266 to
160MHz in the settings! It won't work with the standard 80MHz speed!

6. Compile the code. If there are no errors, upload it to your ESP8266 module

7. Upload the LittleFS Structure (in the "data/" folder of the sketch) to
the filesystem section of your ESP8266 module: Choose "tools -> ESP8266 LittleFS Data Upload"
in your Arduino IDE.

8. Connect to the Serial Monitor and send a request from your Gemini Browser to the
associated Hostname.

You can also use the [makeEspArduino](https://github.com/plerup/makeEspArduino) 
makefile to compile the sketch. Adjust this file accordingly.

## Example Content

### Certificate / Key
*PLEASE CHANGE* the certificate/key data in the config.h file to your 
own certificate. The provided certificate/key is only for testing compilation.

### Gemini Pages
If you want to put your own gemini files on the server, you have to put them
into the "data/gemini/" folder and flash the content to your ESP8266 LittleFS
flash storage.

## Problems / Bugs

### TLS Session Resumption
It seems that the BearSSL Library for the ESP8266 doesn't support TLS Session
Resumption for now, though otherwise stated. A TLS cache would speed the 
following connections significantly up (from ~1800ms to ~200ms).

I'm not sure how to work around this at the moment, maybe it's possible to use
a different library, don't know.

### Path and file path
For now all filenames should have a file extension and no other dot in the whole path
except the one before the extension. E.g. "gemini://gemini:1965/pages/about.gmi".
Otherwise the result is NOT SURE. Maybe the file gets served as octet-stream or maybe
the server returns an error.

Paths like "gemini://gemini:1965/pages/" are allowed (with trailing '/'!!!) and
'gemini://gemini:1965/pages/index.gmi' is served if found, otherwise returns 
'51 File Not Found'. 

If the last part of the path is a directory without trailing '/' then 
'50 Internal Server Error' will be sent by the server.

## TODO
 *  TLS Connection Resumption (TLS Cache)
 *  Add support for different MIME types
 *  Complete and fix resources path parsing
 *  Lots of bugs to iron out
 *  A lot more ...

