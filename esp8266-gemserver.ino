#define F_CPU 160000000L

#include <ESP8266WiFi.h>
#include <time.h>
#include <LittleFS.h>

#include "splitstr.h"
#include "request.h"

#define USE_EC // Use EC Certs. Comment out to use RSA (not recommended)

#include "config.h"

#define		CHUNK_SIZE 		256

const char *ssid = STASSID;
const char *pass = STAPSK;

// Gemini Response Headers
static const char *HDR_GEM_OK 			= "20 text/gemini\r\n";								// .gmi
static const char *HDR_PLAIN_OK 		= "20 text/plain\r\n";								// .txt
static const char *HDR_MARKDOWN_OK	= "20 text/markdown\r\n";							// .md
static const char *HDR_JPEG_OK			= "20 image/jpeg\r\n";								// .jpg
static const char *HDR_BIN_OK				= "20 application/octet-stream\r\n";	// all other content
static const char *HDR_NOT_FOUND 		= "51 File Not Found\r\n";
static const char *HDR_PERM_FAIL 		= "50 Internal Server Error\r\n";

// The TLS server
BearSSL::WiFiServerSecure server(HOSTPORT);

/********************
 * TODO: TLS Session Resumption
 * haven't managed yet to use TLS cache, because the compiler
 * doesn't recognize the used types as "not a type"
 ********************/

void setup() {
	// Start serial port
  Serial.begin(115200);
  Serial.println();
  Serial.println();

	// Set up LittleFS
  LittleFSConfig cfg;
  cfg.setAutoFormat(false);
  LittleFS.setConfig(cfg);
	if (!LittleFS.begin()) {
		Serial.println("LittleFS: Unable to begin(), ABORTING!");
	}

  // Connect to WiFi Network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
	WiFi.hostname(HOSTNAME);
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
	
  // Attach the server private cert/key combo
  BearSSL::X509List *serverCertList = new BearSSL::X509List(server_cert);
  BearSSL::PrivateKey *serverPrivKey = new BearSSL::PrivateKey(server_private_key);
#ifndef USE_EC // use RSA Cert
  server.setRSACert(serverCertList, serverPrivKey);
#else // use EC Cert
	server.setECCert(serverCertList, BR_KEYTYPE_KEYX|BR_KEYTYPE_SIGN, serverPrivKey);
#endif

  // Actually start accepting connections
  server.begin();
}

void loop() {
  static int cnt; // Connection Counter

	// Check for incoming connection
  BearSSL::WiFiClientSecure incoming = server.available();
  if (!incoming) {
    return;
  }

	// Process incoming connection
  Serial.printf("Incoming connection...%d\n",cnt++);

	while (incoming.connected()) {
		if (incoming.available()) {
      // Fetch request string
			String r = incoming.readStringUntil('\r\n');
      // Parse request
			Request req = Request(r);			

			// Print out parsed request data to serial port
      Serial.println("");
      Serial.print("Request: ");
			Serial.println(req.getRequest());
			Serial.print("Protocol: ");
			Serial.println(req.getProtocol());
			Serial.print("Host: ");
			Serial.println(req.getHost());
			Serial.print("Port: ");
			Serial.println(req.getPort());
			Serial.print("Path: ");
			Serial.println(req.getPath());
			Serial.print("Query: ");
			Serial.println(req.getQuery());
			Serial.print("File Path: ");
			Serial.println(req.getFile_path());
			Serial.print("File Ext: ");
			Serial.println(req.getFile_ext());
			
      Serial.print("\nSerching for: ");
      Serial.println(req.getFile_path());
			
			// Check, if requested file exists
			if (!LittleFS.exists(req.getFile_path())) {
				
				// File not found
				
				Serial.print("Header to send: ");
				Serial.write(HDR_NOT_FOUND, strlen(HDR_NOT_FOUND));
				
				// Send File Not Found Header to incoming connection
				incoming.read();
				incoming.write((uint8_t*)HDR_NOT_FOUND, strlen(HDR_NOT_FOUND));
				incoming.flush();
				incoming.stop();

			} else {
				
				// File exists and is a regular file or a dir
				
				// Try to open file on LittleFS and store in buffer
				File file = LittleFS.open(req.getFile_path().c_str(), "r");
				
				if (!file.isFile()) {
					// Error while opening file
					file.close();
					
					Serial.print("Header to send: ");
					Serial.write(HDR_PERM_FAIL, strlen(HDR_PERM_FAIL));
					
					// Send 50 Error header
					incoming.read();
					incoming.write((uint8_t*)HDR_PERM_FAIL, strlen(HDR_PERM_FAIL));
					incoming.flush();
					incoming.stop();

				} else {
          // slurp rest of incoming data
          incoming.read();

          // Send header based on file extension
          Serial.print("Header to send (");
          String e = req.getFile_ext();
          Serial.print(e);
          Serial.print("): ");
          if (e.equals("gmi")) {
                  Serial.write(HDR_GEM_OK, strlen(HDR_GEM_OK));
                  incoming.write((uint8_t*)HDR_GEM_OK, strlen(HDR_GEM_OK));
          } else if (e.equals("txt")) {
                  Serial.write(HDR_PLAIN_OK, strlen(HDR_PLAIN_OK));
                  incoming.write((uint8_t*)HDR_PLAIN_OK, strlen(HDR_PLAIN_OK));
          } else if (e.equals("md")) {
                  Serial.write(HDR_MARKDOWN_OK, strlen(HDR_MARKDOWN_OK));
                  incoming.write((uint8_t*)HDR_MARKDOWN_OK, strlen(HDR_MARKDOWN_OK));
          } else if (e.equals("jpg")) {
                  Serial.write(HDR_JPEG_OK, strlen(HDR_JPEG_OK));
                  incoming.write((uint8_t*)HDR_JPEG_OK, strlen(HDR_JPEG_OK));
          } else {
                  Serial.write(HDR_BIN_OK, strlen(HDR_BIN_OK));
                  incoming.write((uint8_t*)HDR_BIN_OK, strlen(HDR_BIN_OK));
          }

					// read and send file

					// Calculate count of chunks and size of rest
					int fsize = file.size();
					int chunk_count = (fsize / CHUNK_SIZE);
					int rest_size = (fsize % CHUNK_SIZE);

					// read chunks from file and write to incoming connection
					char chunk[CHUNK_SIZE];
					for (int i = 0; i < chunk_count; i++) {
						file.read((uint8_t*)chunk, CHUNK_SIZE);
						incoming.write((uint8_t*)chunk, CHUNK_SIZE);
					}
					// read rest from file and write to incomming connection
					char rest[rest_size];
					file.read((uint8_t*)rest, rest_size);
					incoming.write((uint8_t*)rest, rest_size);

					// close file
					file.close();

					// flush and close connection
					incoming.flush();
					incoming.stop();
				}
			}
		}
	}
  Serial.printf("Connection closed.\n");
}
