# Makefile for ESP8266 Projects
# created for makeEspArduino
# https://github.com/plerup/makeEspArduino
#
# It runs the build process in the
# build/ folder of the current directory.
# The SKETCH is the main .ino file in the
# current directory.

SKETCH = esp8266-gemserver.ino
BUILD_ROOT = build

CHIP = esp8266
BOARD = nodemcuv2
F_CPU = 160000000L
UPLOAD_PORT = /dev/ttyUSB0

FS_TYPE = littlefs
FS_DIR = data

# Include upstream makefile - This has
# to be adjusted to the right path
include $(HOME)/Arduino/makeEspArduino/makeEspArduino.mk

